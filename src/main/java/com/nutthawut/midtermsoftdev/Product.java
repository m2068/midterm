/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nutthawut.midtermsoftdev;

import java.io.Serializable;

/**
 *
 * @author OMEN
 */
public class Product {
    private String id;
    private String name;
    private String brand;
    private String price;
    private String Amount;
    public Product(String id,String name,String brand,String price,String Amount){
        this.id=id;
        this.name=name;
        this.brand=brand;
        this.price=price;
        this.Amount=Amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String band) {
        this.brand = brand;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }
    @Override
    public String toString() {
        return "Id = "+ id + "  Name = "+ name+" Brand = "+brand+
                " Price = "+price+" Amount = "+Amount;
    }
    
}
