/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nutthawut.midtermsoftdev;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OMEN
 */
public class ProductService {

    private static ArrayList<Product> proList = null;

    static {
        proList = new ArrayList<>();
        proList .add(new Product("1" , "Rice" , "easy" , "15.0" , "1"));
        proList .add(new Product("2" , "Sausage" , "Ceepee" , "35.0" , "1"));
        proList .add(new Product("3" , "Snack" , "ArhanYodkhun" , "10.5" , "2"));
       // load();
    }
    //Create

    public static boolean addProduct(Product product) {
        proList.add(product);
        //save();
        return true;
    }
    //Delete

public static boolean delProduct(Product product) {
        proList.remove(product);
       // save();
        return true;
    }
    public static boolean delProduct(int index) {
        proList.remove(index);
        return true;
    }
    //Read

    public static ArrayList<Product> getProducts() {
        return proList;
    }
    public static Product getProduct(int index){
        return proList.get(index);
    }
    //Update

    public static boolean updateProduct(int index, Product product) {
        proList.set(index, product);
        //save();
        return true;
    }
     //public static void save(){
            //File file = null;
            //FileOutputStream fos = null;
            //ObjectOutputStream oos = null;
        //try {
            //file = new File("bank.dat");
            //fos = new FileOutputStream(file);
            //oos = new ObjectOutputStream(fos);
            //oos.writeObject(proList);
          //oos.close();
            //fos.close();
       // } catch (FileNotFoundException ex) {
           // Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
       // } catch (IOException ex) {
           // Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        //}    
    //}
    //public static void load(){
            //File file = null;
          //  FileInputStream fis = null;
           // ObjectInputStream ois = null;
       // try {
            //file = new File("bank.dat");
           // fis = new FileInputStream(file);
           // ois = new ObjectInputStream(fis);
          // proList = (ArrayList<Product>) ois.readObject();
          //  ois.close();
          //  fis.close();
      //  } catch (FileNotFoundException ex) {
        //    Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
      //  } catch (IOException ex) {
        //    Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
      //  } catch (ClassNotFoundException ex) {
         //   Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
      //  }    
        
    }

